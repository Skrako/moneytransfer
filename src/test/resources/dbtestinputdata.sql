--Script for unit test cases (DO NOT CHANGE)

DROP TABLE IF EXISTS User;

CREATE TABLE User (UserId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    UserName VARCHAR(30) NOT NULL,
    EmailAddress VARCHAR(30) NOT NULL
);

CREATE UNIQUE INDEX idx_ue on User(UserName,EmailAddress);

INSERT INTO User (UserName, EmailAddress) VALUES ('peter','peter@wp.pl');
INSERT INTO User (UserName, EmailAddress) VALUES ('dawid','dawid@onet.pl');
INSERT INTO User (UserName, EmailAddress) VALUES ('jacob','jacob@gmail.com');
INSERT INTO User (UserName, EmailAddress) VALUES ('mathew','mathew@hotmail.com');

DROP TABLE IF EXISTS Account;

CREATE TABLE Account (AccountId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    UserName VARCHAR(30),
    Balance DECIMAL(19,4),
    CurrencyCode VARCHAR(30)
);

CREATE UNIQUE INDEX idx_acc on Account(UserName,CurrencyCode);

INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('peter',1000.0000,'EUR');--ID =1
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('dawid',6000.0000,'EUR');--ID =2
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('peter',3000.0000,'GBP');--ID =3
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('dawid',7000.0000,'GBP');--ID =4
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('mathew',500.0000,'GBP');--ID =5
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('peter',2000.0000,'USD');--ID =6
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('dawid',4000.0000,'USD');--ID =7