package com.unittests.moneytransferapp.dao;

import com.moneytransferapp.dao.AccountDAO;
import com.moneytransferapp.dao.FactoryDAO;
import com.moneytransferapp.dao.implementation.H2FactoryDAOImpl;
import com.moneytransferapp.exceptions.CustomException;
import com.moneytransferapp.model.Account;
import com.moneytransferapp.model.Transaction;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CountDownLatch;

import static junit.framework.TestCase.assertEquals;

public class TestAccountBalance {

	private static Logger log = Logger.getLogger(TestAccountDAO.class);

	private static final FactoryDAO H_2_FACTORY_DAO = FactoryDAO.getDAOFactory();
	private static final int THREADS_COUNT = 100;

	@BeforeClass
	public static void setup() {
		//Seeting up database based on src/test/resources/dbtestinputdata.sql
		log.debug("Setting up database for TestAccountBalance");
		H_2_FACTORY_DAO.injectDemoDataIntoDb();
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testAccountSameCurrencyCodeTransferUsingSingleThread() throws CustomException {

		final AccountDAO accountDAO = H_2_FACTORY_DAO.getAccountDAO();
		BigDecimal transferAmount = new BigDecimal(50.01234).setScale(4, RoundingMode.HALF_EVEN);
		Transaction transaction = new Transaction("GBP", transferAmount, 3L, 4L);

		long startTime = System.currentTimeMillis();
		accountDAO.transferAccountBalance(transaction);
		long endTime = System.currentTimeMillis();

		log.info("TransferAccountBalance finished, time taken: " + (endTime - startTime) + "ms");

		Account accountFrom = accountDAO.getAccountById(3);
		Account accountTo = accountDAO.getAccountById(4);

		log.debug("Account from: " + accountFrom);
		log.debug("Account from: " + accountTo);

		assertEquals(1, accountFrom.getBalance().compareTo(new BigDecimal(449.9877).setScale(4, RoundingMode.HALF_EVEN)));
		assertEquals(accountTo.getBalance(), new BigDecimal(7050.0123).setScale(4, RoundingMode.HALF_EVEN));
	}

	@Test
	public void testAccountTransferUsingMultiThreading() throws InterruptedException, CustomException {

		final AccountDAO accountDAO = H_2_FACTORY_DAO.getAccountDAO();
		final CountDownLatch latch = new CountDownLatch(THREADS_COUNT);

		for (int i = 0; i < THREADS_COUNT; ++i) {
			new Thread(() -> {
				try {
					Transaction transaction = new Transaction("USD", new BigDecimal(10).setScale(4, RoundingMode.HALF_EVEN), 1L, 2L);
					accountDAO.transferAccountBalance(transaction);
				} catch (Exception e) {
					log.error("Error occurred during transfer ", e);
				} finally {
					latch.countDown();
				}
			}).start();
		}

		latch.await();

		Account accountFrom = accountDAO.getAccountById(1);
		Account accountTo = accountDAO.getAccountById(2);

		log.debug("Account from: " + accountFrom);
		log.debug("Account from: " + accountTo);

		assertEquals(accountFrom.getBalance(), new BigDecimal(1000).setScale(4, RoundingMode.HALF_EVEN));
		assertEquals(accountTo.getBalance(), new BigDecimal(6000).setScale(4, RoundingMode.HALF_EVEN));
	}

	@Test
	public void testTransferFailOnDataBaseLockUsingSingleThread() throws CustomException {
		final String SQL_LOCK_ACCOUNT = "SELECT * FROM Account WHERE AccountId = 5 FOR UPDATE";
		Connection conn = null;
		PreparedStatement lockStmt = null;
		ResultSet rs = null;
		Account fromAccount = null;

		try {
			conn = H2FactoryDAOImpl.getConnection();
			conn.setAutoCommit(false);
			// lock db account for writing:
			lockStmt = conn.prepareStatement(SQL_LOCK_ACCOUNT);
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				fromAccount = new Account(rs.getLong("AccountId"), rs.getString("UserName"),
						rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("Locked account: " + fromAccount);
			}

			if (fromAccount == null) {
				throw new CustomException("Locking error during test, SQL = " + SQL_LOCK_ACCOUNT);
			}
			//account 5 locked, try to transfer from account 6 to 5
			//h2 default timeout for acquire lock is 1 sec
			BigDecimal transferAmount = new BigDecimal(50).setScale(4, RoundingMode.HALF_EVEN);

			Transaction transaction = new Transaction("GBP", transferAmount, 6L, 5L);
			H_2_FACTORY_DAO.getAccountDAO().transferAccountBalance(transaction);
			conn.commit();
		} catch (Exception e) {
			log.error("Exception occurred, initiate a rollback");
			try {
				if (conn != null)
					conn.rollback();
			} catch (SQLException re) {
				log.error("Fail to rollback transaction", re);
			}
		} finally {
			DbUtils.closeQuietly(conn);
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(lockStmt);
		}

		//verify if no transaction occurred
		BigDecimal accountId5OriginalBalance = new BigDecimal(500).setScale(4, RoundingMode.HALF_EVEN);
		assertEquals(H_2_FACTORY_DAO.getAccountDAO().getAccountById(5).getBalance(), accountId5OriginalBalance);
		BigDecimal accountId6OriginalBalance = new BigDecimal(2000).setScale(4, RoundingMode.HALF_EVEN);
		assertEquals(H_2_FACTORY_DAO.getAccountDAO().getAccountById(6).getBalance(), accountId6OriginalBalance);
	}

}
