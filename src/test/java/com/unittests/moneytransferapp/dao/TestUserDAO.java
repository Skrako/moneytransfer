package com.unittests.moneytransferapp.dao;

import com.moneytransferapp.dao.FactoryDAO;
import com.moneytransferapp.exceptions.CustomException;
import com.moneytransferapp.model.User;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.*;

public class TestUserDAO {
	
	private static Logger log = Logger.getLogger(TestUserDAO.class);
	
	private static final FactoryDAO H_2_FACTORY_DAO = FactoryDAO.getDAOFactory();

	@BeforeClass
	public static void setup() {
		log.debug("Setting up database for TestUserDAO");
		H_2_FACTORY_DAO.injectDemoDataIntoDb();
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testCreateUser() throws CustomException {
		User u = new User("liandre", "liandre@gmail.com");
		long id = H_2_FACTORY_DAO.getUserDAO().insertUser(u);
		User uAfterInsert = H_2_FACTORY_DAO.getUserDAO().getUserById(id);
		assertEquals("liandre", uAfterInsert.getUserName());
		assertEquals("liandre@gmail.com", u.getEmailAddress());
	}

	@Test
	public void testUpdateUser() throws CustomException {
		User u = new User(1L, "test2", "test2@gmail.com");
		int rowCount = H_2_FACTORY_DAO.getUserDAO().updateUser(1L, u);
		assertEquals(1, rowCount);
		assertEquals("test2@gmail.com", H_2_FACTORY_DAO.getUserDAO().getUserById(1L).getEmailAddress());
	}

	@Test
	public void testUpdateNonExistingUser() throws CustomException {
		User u = new User(500L, "test2", "test2@gmail.com");
		int rowCount = H_2_FACTORY_DAO.getUserDAO().updateUser(500L, u);
		assertEquals(0, rowCount);
	}

	@Test
	public void testGetUserById() throws CustomException {
		User u = H_2_FACTORY_DAO.getUserDAO().getUserById(2L);
		assertEquals("dawid", u.getUserName());
	}

	@Test
	public void testGetNonExistingUserById() throws CustomException {
		User u = H_2_FACTORY_DAO.getUserDAO().getUserById(500L);
		assertNull(u);
	}

	@Test
	public void testGetNonExistingUserByName() throws CustomException {
		User u = H_2_FACTORY_DAO.getUserDAO().getUserByName("abcdeftg");
		assertNull(u);
	}

	@Test
	public void testGetAllUsers() throws CustomException {
		List<User> allUsers = H_2_FACTORY_DAO.getUserDAO().getAllUsers();
		assertTrue(allUsers.size() > 1);
	}

	@Test
	public void testDeleteUser() throws CustomException {
		int rowCount = H_2_FACTORY_DAO.getUserDAO().deleteUser(1L);
		assertEquals(1, rowCount);
		assertNull(H_2_FACTORY_DAO.getUserDAO().getUserById(1L));
	}

	@Test
	public void testDeleteNonExistingUser() throws CustomException {
		int rowCount = H_2_FACTORY_DAO.getUserDAO().deleteUser(500L);
		assertEquals(0, rowCount);
	}

}
