package com.unittests.moneytransferapp.dao;

import com.moneytransferapp.dao.FactoryDAO;
import com.moneytransferapp.exceptions.CustomException;
import com.moneytransferapp.model.Account;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static junit.framework.TestCase.*;

public class TestAccountDAO {

	private static Logger log = Logger.getLogger(TestAccountDAO.class);

	private static final FactoryDAO H_2_FACTORY_DAO = FactoryDAO.getDAOFactory();

	@BeforeClass
	public static void setup() {
		//Seeting up database based on src/test/resources/dbtestinputdata.sql
		log.debug("Setting up database for TestAccountDAO");
		H_2_FACTORY_DAO.injectDemoDataIntoDb();
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testCreateAccount() throws CustomException {
		BigDecimal balance = new BigDecimal(10).setScale(4, RoundingMode.HALF_EVEN);
		Account a = new Account("newUserName", balance, "PLN");
		long aid = H_2_FACTORY_DAO.getAccountDAO().createAccount(a);
		Account afterCreation = H_2_FACTORY_DAO.getAccountDAO().getAccountById(aid);
		assertEquals("newUserName", afterCreation.getUserName());
		assertEquals("PLN", afterCreation.getCurrencyCode());
		assertEquals(afterCreation.getBalance(), balance);
	}

	@Test
	public void testUpdateAccountBalanceSufficientFund() throws CustomException {

		BigDecimal deltaDeposit = new BigDecimal(50).setScale(4, RoundingMode.HALF_EVEN);
		BigDecimal afterDeposit = new BigDecimal(1050).setScale(4, RoundingMode.HALF_EVEN);
		int rowsUpdated = H_2_FACTORY_DAO.getAccountDAO().updateAccountBalance(1L, deltaDeposit);
		assertEquals(1, rowsUpdated);
		assertEquals(H_2_FACTORY_DAO.getAccountDAO().getAccountById(1L).getBalance(), afterDeposit);
		BigDecimal deltaWithDraw = new BigDecimal(-50).setScale(4, RoundingMode.HALF_EVEN);
		int rowsUpdatedW = H_2_FACTORY_DAO.getAccountDAO().updateAccountBalance(1L, deltaWithDraw);
		assertEquals(1, rowsUpdatedW);
		BigDecimal afterWithDraw = new BigDecimal(1000).setScale(4, RoundingMode.HALF_EVEN);
		assertEquals(H_2_FACTORY_DAO.getAccountDAO().getAccountById(1L).getBalance(), afterWithDraw);
	}

	@Test(expected = CustomException.class)
	public void testUpdateAccountBalanceNotEnoughFund() throws CustomException {
		BigDecimal deltaWithDraw = new BigDecimal(-50000).setScale(4, RoundingMode.HALF_EVEN);
		int rowsUpdatedW = H_2_FACTORY_DAO.getAccountDAO().updateAccountBalance(1L, deltaWithDraw);
		assertEquals(0, rowsUpdatedW);
	}

	@Test
	public void testGetAccountById() throws CustomException {
		Account account = H_2_FACTORY_DAO.getAccountDAO().getAccountById(1L);
		assertEquals("peter", account.getUserName());
	}

	@Test
	public void testGetNonExistingAccountById() throws CustomException {
		Account account = H_2_FACTORY_DAO.getAccountDAO().getAccountById(100L);
		assertNull(account);
	}

	@Test
	public void testGetAllAccounts() throws CustomException {
		List<Account> allAccounts = H_2_FACTORY_DAO.getAccountDAO().getAllAccounts();
		assertTrue(allAccounts.size() > 1);
	}

	@Test
	public void testDeleteAccount() throws CustomException {
		int rowCount = H_2_FACTORY_DAO.getAccountDAO().deleteAccountById(2L);
		assertEquals(1, rowCount);
		assertNull(H_2_FACTORY_DAO.getAccountDAO().getAccountById(2L));
	}

	@Test
	public void testDeleteNonExistingAccount() throws CustomException {
		int rowCount = H_2_FACTORY_DAO.getAccountDAO().deleteAccountById(500L);
		assertEquals(0, rowCount);
	}

}