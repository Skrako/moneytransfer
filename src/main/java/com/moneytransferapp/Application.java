package com.moneytransferapp;

import com.moneytransferapp.dao.FactoryDAO;
import com.moneytransferapp.services.AccountService;
import com.moneytransferapp.services.ServiceExceptionMapper;
import com.moneytransferapp.services.TransactionService;
import com.moneytransferapp.services.UserService;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class Application {

	private static Logger log = Logger.getLogger(Application.class);

	public static void main(String[] args) throws Exception {
		log.info("Starting server");
		FactoryDAO h2FactoryDAO = FactoryDAO.getDAOFactory();
		log.info("Setting up and injecting data into database");
		h2FactoryDAO.injectDemoDataIntoDb();
		log.info("Service started");
		startService();
	}

	private static void startService() throws Exception {
		Server server = new Server(8080);
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		server.setHandler(context);
		ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
		servletHolder.setInitParameter("jersey.config.server.provider.classnames",UserService.class.getCanonicalName() + "," + AccountService.class.getCanonicalName() + "," + ServiceExceptionMapper.class.getCanonicalName() + "," + TransactionService.class.getCanonicalName());
		try {
			server.start();
			server.join();
		} finally {
			server.destroy();
		}
	}

}
