package com.moneytransferapp.services;

import com.moneytransferapp.dao.FactoryDAO;
import com.moneytransferapp.exceptions.CustomException;
import com.moneytransferapp.model.User;

import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService {

    private static Logger log = Logger.getLogger(UserService.class);

    private final FactoryDAO factoryDAO = FactoryDAO.getDAOFactory();

    @POST
    @Path("/create")
    public User createUser(User user) throws CustomException {
        if (factoryDAO.getUserDAO().getUserByName(user.getUserName()) != null) {
            throw new WebApplicationException("User name already exist", Response.Status.BAD_REQUEST);
        }
        final long uId = factoryDAO.getUserDAO().insertUser(user);
        return factoryDAO.getUserDAO().getUserById(uId);
    }

    @GET
    @Path("/{userName}")
    public User getUserByName(@PathParam("userName") String userName) throws CustomException {
        if (log.isDebugEnabled())
            log.debug("Request Received for get User by Name " + userName);
        final User user = factoryDAO.getUserDAO().getUserByName(userName);
        if (user == null) {
            throw new WebApplicationException("User Not Found", Response.Status.NOT_FOUND);
        }
        return user;
    }
    
    @GET
    @Path("/all")
    public List<User> getAllUsers() throws CustomException {
        return factoryDAO.getUserDAO().getAllUsers();
    }

    @PUT
    @Path("/{userId}")
    public Response updateUser(@PathParam("userId") long userId, User user) throws CustomException {
        final int updateCount = factoryDAO.getUserDAO().updateUser(userId, user);
        if (updateCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @DELETE
    @Path("/{userId}")
    public Response deleteUser(@PathParam("userId") long userId) throws CustomException {
        int deleteCount = factoryDAO.getUserDAO().deleteUser(userId);
        if (deleteCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
