package com.moneytransferapp.services;

import com.moneytransferapp.dao.FactoryDAO;
import com.moneytransferapp.exceptions.CustomException;
import com.moneytransferapp.model.Account;
import com.moneytransferapp.model.MoneyUtil;

import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountService {

    private static Logger log = Logger.getLogger(AccountService.class);

    private final FactoryDAO factoryDAO = FactoryDAO.getDAOFactory();

    @PUT
    @Path("/create")
    public Account createAccount(Account account) throws CustomException {
        final long accountId = factoryDAO.getAccountDAO().createAccount(account);
        return factoryDAO.getAccountDAO().getAccountById(accountId);
    }

    @GET
    @Path("/all")
    public List<Account> getAllAccounts() throws CustomException {
        return factoryDAO.getAccountDAO().getAllAccounts();
    }

    @GET
    @Path("/{accountId}")
    public Account getAccount(@PathParam("accountId") long accountId) throws CustomException {
        return factoryDAO.getAccountDAO().getAccountById(accountId);
    }
    
    @GET
    @Path("/{accountId}/balance")
    public BigDecimal getBalance(@PathParam("accountId") long accountId) throws CustomException {
        final Account account = factoryDAO.getAccountDAO().getAccountById(accountId);

        if(account == null){
            throw new WebApplicationException("Account not found", Response.Status.NOT_FOUND);
        }
        return account.getBalance();
    }

    @PUT
    @Path("/{accountId}/deposit/{amount}")
    public Account deposit(@PathParam("accountId") long accountId,@PathParam("amount") BigDecimal amount) throws CustomException {

        if (amount.compareTo(MoneyUtil.zeroAmount) <=0){
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }

        factoryDAO.getAccountDAO().updateAccountBalance(accountId,amount.setScale(4, RoundingMode.HALF_EVEN));
        return factoryDAO.getAccountDAO().getAccountById(accountId);
    }

    @PUT
    @Path("/{accountId}/withdraw/{amount}")
    public Account withdraw(@PathParam("accountId") long accountId,@PathParam("amount") BigDecimal amount) throws CustomException {

        if (amount.compareTo(MoneyUtil.zeroAmount) <=0){
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }
        BigDecimal delta = amount.negate();
        if (log.isDebugEnabled())
            log.debug("Withdraw services: delta change to account  " + delta + " Account ID = " +accountId);
        factoryDAO.getAccountDAO().updateAccountBalance(accountId,delta.setScale(4, RoundingMode.HALF_EVEN));
        return factoryDAO.getAccountDAO().getAccountById(accountId);
    }

    @DELETE
    @Path("/{accountId}")
    public Response deleteAccount(@PathParam("accountId") long accountId) throws CustomException {
        int deleteCount = factoryDAO.getAccountDAO().deleteAccountById(accountId);
        if (deleteCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
