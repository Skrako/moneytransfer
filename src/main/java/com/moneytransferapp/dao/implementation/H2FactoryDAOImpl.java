package com.moneytransferapp.dao.implementation;

import com.moneytransferapp.dao.AccountDAO;
import com.moneytransferapp.dao.FactoryDAO;
import com.moneytransferapp.dao.UserDAO;
import com.moneytransferapp.utils.Utils;

import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;
import org.h2.tools.RunScript;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2FactoryDAOImpl extends FactoryDAO {

	private static Logger log = Logger.getLogger(H2FactoryDAOImpl.class);

	private static final String h2_driver = Utils.getStringProperty("h2_driver");
	private static final String h2_connection_url = Utils.getStringProperty("h2_connection_url");
	private static final String h2_user = Utils.getStringProperty("h2_user");
	private static final String h2_password = Utils.getStringProperty("h2_password");

	private final UserDAOImpl userDAO = new UserDAOImpl();
	private final AccountDAOImpl accountDAO = new AccountDAOImpl();

	public H2FactoryDAOImpl() {
		DbUtils.loadDriver(h2_driver);
	}

	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(h2_connection_url, h2_user, h2_password);
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public AccountDAO getAccountDAO() {
		return accountDAO;
	}

	@Override
	public void injectDemoDataIntoDb() {
		log.info("Injecting test users and data into database");
		Connection conn = null;
		try {
			conn = H2FactoryDAOImpl.getConnection();
			RunScript.execute(conn, new FileReader("src/test/resources/dbtestinputdata.sql"));
		} catch (SQLException e) {
			log.error("injectDemoDataIntoDb(): Error injecting data into database: ", e);
			throw new RuntimeException(e);
		} catch (FileNotFoundException e) {
			log.error("injectDemoDataIntoDb(): Error finding dbtestinputdata.sql script file for injecting data into database", e);
			throw new RuntimeException(e);
		} finally {
			DbUtils.closeQuietly(conn);
		}
	}

}
