package com.moneytransferapp.dao.implementation;

import com.moneytransferapp.dao.AccountDAO;
import com.moneytransferapp.exceptions.CustomException;
import com.moneytransferapp.model.Account;
import com.moneytransferapp.model.MoneyUtil;
import com.moneytransferapp.model.Transaction;

import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountDAOImpl implements AccountDAO {

	private static Logger log = Logger.getLogger(AccountDAOImpl.class);

	private final static String SQL_CREATE_ACCOUNT = "INSERT INTO Account (UserName, Balance, CurrencyCode) VALUES (?, ?, ?)";
	private final static String SQL_UPDATE_ACCOUNT_BALANCE = "UPDATE Account SET Balance = ? WHERE AccountId = ? ";
	private final static String SQL_DELETE_ACCOUNT_BY_ID = "DELETE FROM Account WHERE AccountId = ?";
	private final static String SQL_GET_ACCOUNT_BY_ID = "SELECT * FROM Account WHERE AccountId = ? ";
	private final static String SQL_LOCK_ACCOUNT_BY_ID = "SELECT * FROM Account WHERE AccountId = ? FOR UPDATE";
	private final static String SQL_GET_ALL_ACCOUNT = "SELECT * FROM Account";

	public long createAccount(Account account) throws CustomException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet generatedKeys = null;
		try {
			conn = H2FactoryDAOImpl.getConnection();
			stmt = conn.prepareStatement(SQL_CREATE_ACCOUNT);
			stmt.setString(1, account.getUserName());
			stmt.setBigDecimal(2, account.getBalance());
			stmt.setString(3, account.getCurrencyCode());
			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 0) {
				log.error("createAccount(Account account): Creating account failed");
				throw new CustomException("Creating account failed");
			}
			generatedKeys = stmt.getGeneratedKeys();
			if (generatedKeys.next()) {
				return generatedKeys.getLong(1);
			} else {
				log.error("createAccount(Account account): Creating account failed, no ID obtained.");
				throw new CustomException("Creating account failed");
			}
		} catch (SQLException e) {
			log.error("Error creating account " + account);
			throw new CustomException("createAccount(Account account): Error creating user account" + account, e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, generatedKeys);
		}
	}

	public List<Account> getAllAccounts() throws CustomException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Account> allAccounts = new ArrayList<>();
		try {
			conn = H2FactoryDAOImpl.getConnection();
			stmt = conn.prepareStatement(SQL_GET_ALL_ACCOUNT);
			rs = stmt.executeQuery();
			while (rs.next()) {
				Account acc = new Account(rs.getLong("AccountId"), rs.getString("UserName"),
						rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("getAllAccounts(): Get Account " + acc);
				allAccounts.add(acc);
			}
			return allAccounts;
		} catch (SQLException e) {
			throw new CustomException("getAccountById(): Error getting all accounts", e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, rs);
		}
	}
	
	public Account getAccountById(long accountId) throws CustomException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Account acc = null;
		try {
			conn = H2FactoryDAOImpl.getConnection();
			stmt = conn.prepareStatement(SQL_GET_ACCOUNT_BY_ID);
			stmt.setLong(1, accountId);
			rs = stmt.executeQuery();
			if (rs.next()) {
				acc = new Account(rs.getLong("AccountId"), rs.getString("UserName"), rs.getBigDecimal("Balance"),
						rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("Get account by id: " + acc);
			}
			return acc;
		} catch (SQLException e) {
			throw new CustomException("getAccountById(long accountId): Error getting account data", e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, rs);
		}

	}

	public int deleteAccountById(long accountId) throws CustomException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = H2FactoryDAOImpl.getConnection();
			stmt = conn.prepareStatement(SQL_DELETE_ACCOUNT_BY_ID);
			stmt.setLong(1, accountId);
			return stmt.executeUpdate();
		} catch (SQLException e) {
			throw new CustomException("deleteAccountById(long accountId): Error deleting account by account Id " + accountId, e);
		} finally {
			DbUtils.closeQuietly(conn);
			DbUtils.closeQuietly(stmt);
		}
	}

	public int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws CustomException {
		Connection conn = null;
		PreparedStatement lockStmt = null;
		PreparedStatement updateStmt = null;
		ResultSet rs = null;
		Account targetAccount = null;
		int updateCount = -1;
		try {
			conn = H2FactoryDAOImpl.getConnection();
			conn.setAutoCommit(false);
			//lock account in db for writing
			lockStmt = conn.prepareStatement(SQL_LOCK_ACCOUNT_BY_ID);
			lockStmt.setLong(1, accountId);
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				targetAccount = new Account(rs.getLong("AccountId"), rs.getString("UserName"),
						rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("updateAccountBalance(long accountId, BigDecimal deltaAmount) from Account: " + targetAccount);
			}

			if (targetAccount == null) {
				throw new CustomException("updateAccountBalance(long accountId, BigDecimal deltaAmount): fail to lock account : " + accountId);
			}
			//update account after success db account locking
			BigDecimal balance = targetAccount.getBalance().add(deltaAmount);
			if (balance.compareTo(MoneyUtil.zeroAmount) < 0) {
				throw new CustomException("Not enough fund for account: " + accountId);
			}

			updateStmt = conn.prepareStatement(SQL_UPDATE_ACCOUNT_BALANCE);
			updateStmt.setBigDecimal(1, balance);
			updateStmt.setLong(2, accountId);
			updateCount = updateStmt.executeUpdate();
			conn.commit();
			if (log.isDebugEnabled())
				log.debug("New balance after update: " + targetAccount);
			return updateCount;
		} catch (SQLException se) {
			log.error("updateAccountBalance(long accountId, BigDecimal deltaAmount): User transaction failed. Rollback initiated for: " + accountId, se);
			try {
				if (conn != null)
					conn.rollback();
			} catch (SQLException re) {
				throw new CustomException("Fail to rollback transaction", re);
			}
		} finally {
			cleanUp(conn, lockStmt, updateStmt, rs);
		}
		return updateCount;
	}

	public int transferAccountBalance(Transaction transaction) throws CustomException {

		if (transaction.getAmount().compareTo(BigDecimal.ZERO) <= 0){
			throw new CustomException("transferAccountBalance(Transaction transaction): Negative amount not allowed");
		}

		int result = -1;
		Connection conn = null;
		PreparedStatement lockStmt = null;
		PreparedStatement updateStmt = null;
		ResultSet rs = null;
		Account fromAccount = null;
		Account toAccount = null;

		try {
			conn = H2FactoryDAOImpl.getConnection();
			conn.setAutoCommit(false);
			// lock the credit and debit account for writing:
			lockStmt = conn.prepareStatement(SQL_LOCK_ACCOUNT_BY_ID);
			lockStmt.setLong(1, transaction.getFromAccountId());
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				fromAccount = new Account(rs.getLong("AccountId"), rs.getString("UserName"),
						rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("transferAccountBalance from Account: " + fromAccount);
			}
			lockStmt = conn.prepareStatement(SQL_LOCK_ACCOUNT_BY_ID);
			lockStmt.setLong(1, transaction.getToAccountId());
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				toAccount = new Account(rs.getLong("AccountId"), rs.getString("UserName"), rs.getBigDecimal("Balance"),
						rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("transferAccountBalance to Account: " + toAccount);
			}

			// check locking status
			if (fromAccount == null || toAccount == null) {
				throw new CustomException("transferAccountBalance(Transaction transaction): Fail to lock both accounts for write");
			}

			// check transaction currency
			if (!fromAccount.getCurrencyCode().equals(transaction.getCurrencyCode())) {
				throw new CustomException("transferAccountBalance(Transaction transaction): Fail to transfer Fund, transaction ccy are different from source/destination");
			}

			// check ccy is the same for both accounts
			if (!fromAccount.getCurrencyCode().equals(toAccount.getCurrencyCode())) {
				throw new CustomException("transferAccountBalance(Transaction transaction): Fail to transfer Fund, the source and destination account are in different currency");
			}

			// check enough fund in source account
			BigDecimal fromAccountLeftOver = fromAccount.getBalance().subtract(transaction.getAmount());
			if (fromAccountLeftOver.compareTo(MoneyUtil.zeroAmount) < 0) {
				throw new CustomException("transferAccountBalance(Transaction transaction): Not enough fund from source account");
			}
			// proceed with update
			updateStmt = conn.prepareStatement(SQL_UPDATE_ACCOUNT_BALANCE);
			updateStmt.setBigDecimal(1, fromAccountLeftOver);
			updateStmt.setLong(2, transaction.getFromAccountId());
			updateStmt.addBatch();
			updateStmt.setBigDecimal(1, toAccount.getBalance().add(transaction.getAmount()));
			updateStmt.setLong(2, transaction.getToAccountId());
			updateStmt.addBatch();
			int[] rowsUpdated = updateStmt.executeBatch();
			result = rowsUpdated[0] + rowsUpdated[1];
			if (log.isDebugEnabled()) {
				log.debug("Number of rows updated for the transfer: " + result);
			}
			// If there is no error, commit the transaction
			conn.commit();
		} catch (SQLException se) {
			// rollback transaction if exceptions occurs
			log.error("transferAccountBalance(Transaction transaction): User Transaction Failed, rollback initiated for: " + transaction, se);
			try {
				if (conn != null) {
					conn.rollback();
				}
			} catch (SQLException re) {
				throw new CustomException("Fail to rollback transaction", re);
			}
		} finally {
			cleanUp(conn, lockStmt, updateStmt, rs);
		}
		return result;
	}

	public void cleanUp(Connection conn, PreparedStatement lockStmt, PreparedStatement updateStmt, ResultSet rs) {
		DbUtils.closeQuietly(conn);
		DbUtils.closeQuietly(rs);
		DbUtils.closeQuietly(lockStmt);
		DbUtils.closeQuietly(updateStmt);
	}

}
