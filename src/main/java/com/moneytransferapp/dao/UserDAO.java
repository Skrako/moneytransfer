package com.moneytransferapp.dao;

import com.moneytransferapp.exceptions.CustomException;
import com.moneytransferapp.model.User;

import java.util.List;

public interface UserDAO {

	long insertUser(User user) throws CustomException;
	int updateUser(Long userId, User user) throws CustomException;
	int deleteUser(long userId) throws CustomException;

	User getUserById(long userId) throws CustomException;
	User getUserByName(String userName) throws CustomException;
	List<User> getAllUsers() throws CustomException;
}
