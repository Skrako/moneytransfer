package com.moneytransferapp.dao;

import com.moneytransferapp.exceptions.CustomException;
import com.moneytransferapp.model.Account;
import com.moneytransferapp.model.Transaction;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;


public interface AccountDAO {

    long createAccount(Account account) throws CustomException;
    List<Account> getAllAccounts() throws CustomException;
    Account getAccountById(long accountId) throws CustomException;
    int deleteAccountById(long accountId) throws CustomException;

    int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws CustomException;
    int transferAccountBalance(Transaction transaction) throws CustomException;

    void cleanUp(Connection conn, PreparedStatement lockStmt, PreparedStatement updateStmt, ResultSet rs);
}
