package com.moneytransferapp.dao;

import com.moneytransferapp.dao.implementation.H2FactoryDAOImpl;

public abstract class FactoryDAO {

	public abstract UserDAO getUserDAO();
	public abstract AccountDAO getAccountDAO();
	public abstract void injectDemoDataIntoDb();
	public static FactoryDAO getDAOFactory() {
		return new H2FactoryDAOImpl();
	}
}
