### Money Transfer Application Rest API
Java RESTful API (including data model and the backing implementation) for money transfers between accounts
Application starts a jetty server on localhost port 8080 and using H2 in memory database which is initialized with some sample data

### Technologies
- JAX-RS
- H2 in memory database
- Log4j
- Jetty Container
- Apache HTTP Client
- Maven

### Available Services
| HTTP METHOD | PATH | USAGE |

| PUT | /user/create | create a new user |

| PUT | /account/create | create a new account

| PUT | /account/{accountId}/withdraw/{amount} | withdraw money from account |

| PUT | /account/{accountId}/deposit/{amount} | deposit money to account |

| GET | /user/{userName} | get user by user name |

| GET | /user/all | get all users |

| POST | /user/{userId} | update user |

| POST | /transaction | perform transaction between 2 user accounts |

| DELETE | /user/{userId} | remove user |

| DELETE | /account/{accountId} | remove account by accountId |

| GET | /account/{accountId} | get account by accountId |

| GET | /account/all | get all accounts |

| GET | /account/{accountId}/balance | get account balance by accountId |

### Http Status
- 200 OK: The request has succeeded
- 400 Bad Request: The request could not be understood by the server
- 404 Not Found: The requested resource cannot be found
- 500 Internal Server Error: The server encountered an unexpected condition